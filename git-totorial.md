﻿<h1>git学习笔记</h1>
<h2>提供人：郭都豪（大二大数据）</h2>

## 目录
* [本地操作](#1)
	* [初始化一个git仓库](#1.1)
	* [将文件加入/撤出暂存区](#1.2)
	* [提交至本地仓库（HEAD）](#1.4)
	* [回滚本地更改](#1.5)
	* [忽略文件](#1.6)
	* [查看差异](#1.7)
	* [基本命令](#1.8)
* [远程操作](#2)
	* [使用GitHub](#2.0)
	* [将本地仓库关联到远程仓库（GitHub仓库）](#2.1)
	* [推送到远程仓库](#2.2)
	* [从远程仓库克隆代码至本地仓库](#2.3)
	* [从远程仓库拉取代码至工作区](#2.4)
	* [处理冲突](#2.5)
* [分支](#3)
	* [创建/删除/切换分支](#3.1)
	* [合并某分支至当前分支](#3.2)

### git流程图
![](https://www.lynx.im/wp-content/uploads/2016/08/0df9740f9b41522ab12c14dbbf6bbad4_r.png)
<span id="1"></span>
### 本地操作
<span id="1.1"/>
#### 初始化一个git仓库
```
git init
```
<span id="1.2"/>
#### 将文件加入/撤出暂存区
```
git add .
```
这个命令会将目录下所有文件加入git仓库，如果使用`git add *`，则只会加入当前目录下的文件。
```
git reset HEAD <文件>
```
将暂存区中的更改撤回至工作区，用于回滚add操作。
<!--more-->
<span id="1.4"/>
#### 提交至本地仓库（HEAD）
```
git commit -m"commit message"
```
如果出现changes not staged for commit之类的错误，重新执行`git add .`
<span id="1.5"/>
#### 回滚本地更改
将工作区中的更改回滚至HEAD
```
git checkout -- <文件>
```
<span id="1.6"/>
#### 忽略文件
有些时候，你必须把某些文件放到Git工作目录中，但又不能提交它们，比如保存了数据库密码的配置文件，或是编译生成的一些文件。对于这类文件，我们可以在.gitignore中将他们排除。

对于要排除的文件，只需要将文件名写入`.gitignore`即可。除此之外，GitHub还提供了每一种语言的`.gitignore`模板：[](https://github.com/github/gitignore) 。

之后在执行`git add`时，git会提示有哪些文件根据`.gitignore`文件的规则被排除了。
<span id="1.7"/>
#### 查看差异
比对暂存区与工作区。
```
git diff
```
比对工作区与当前分支最新commit（HEAD）
```
git diff HEAD
```
比对两次提交
```
git diff bfe12d0d31a8f41376e5cfa04f6eb80ade7dcc6d 9b3708ff25b81050e941058a447e1cb19f67531e
```
<span id="1.8"/>
#### 基本命令
```
git log
git status
```
<span id="2"></span>
### 远程操作
<span id="2.0"></span>
#### 使用GitHub
首先需要把电脑的ssh key添加到GitHub
```
ssh-keygen -t rsa -C "你的邮箱"
```
如果一切都保持默认的话，在`~/.ssh`目录中会有`id_rsa`和`id_rsa.pub`，前者是私钥，后者是公钥。

打开GitHub→Settings→SSH Keys，点击New SSH Key，将id_rsa.pub中的内容粘贴进去。

然后在GitHub新建一个repo。
<span id="2.1"></span>
#### 将本地仓库关联到远程仓库（GitHub仓库）
```
git remote add origin git@github.com:yourname/xxx.git
```
其中origin代表远程仓库的名字，可以自己起。
<span id="2.2"></span>
#### 推送到远程仓库
```
git push -u origin master
```
第一次推送master分支时，加上-u参数，Git会把本地的master分支内容推送到远程新的master分支，同时把本地的master分支和远程的master分支关联起来，在以后的推送或者拉取时就可以简化命令，使用：
```
git push origin master
git pull
```
你可以把 master 换成你想要推送的任何分支
关于-u参数的解释详见http://stackoverflow.com/questions/5697750/what-exactly-does-the-u-do-git-push-u-origin-master-vs-git-push-origin-ma
在推送的时候，可能会出现类似这样的错误。
>提示：更新被拒绝，因为远程版本库包含您本地尚不存在的提交。这通常是因为另外
提示：一个版本库已向该引用进行了推送。再次推送前，您可能需要先整合远程变更
提示：（如 'git pull ...'）。

如字面意思，需要先使用`git pull`整合远程仓库的变更。具体做法请查看 [从远程仓库拉取代码至工作区]()
也可以使用+master强制推送
```
git push -u origin +master
```
<span id="2.3"></span>
#### 从远程仓库克隆代码至本地仓库
```
git clone git@github.com:yourname/xxx.git
```
<span id="2.4"></span>
#### 从远程仓库拉取代码至工作区
```
git pull origin master
```
*pull*等价于先*fetch*再*merge*
在合并时git会尝试自动合并，如果合并不成功则出现*冲突*。
举个栗子，假如本地仓库有`1`,`2`两个文件，远程仓库有`1`,`2`,`3`三个文件，拉取时git将自动把`3`合并至本地仓库。
如果远程仓库和本地同时修改了文件`1`的同一处，则会产生冲突，需要手动处理。
<span id="2.5"></span>
#### 处理冲突
>自动合并 1.txt
冲突（内容）：合并冲突于 1.txt
自动合并失败，修正冲突然后提交修正的结果。

发生冲突的文件形如：
```
<<<<<<< HEAD
3hahahaha
=======
3333333
>>>>>>> 45b944792e75cffb62d5b9c53eac3bb363ce2448
```
分割线上方为当前HEAD的更改，下方为远程仓库commit id 的更改。编辑发生冲突的文件，消除冲突。
>您的分支和 'origin/master' 出现了偏离，
并且分别有 1 和 1 处不同的提交。
（使用 "git pull" 来合并远程分支）
所有冲突已解决但您仍处于合并中。
（使用 "git commit" 结束合并）

执行`git commit`结束合并。
>您的分支领先 'origin/master' 共 2 个提交。
（使用 "git push" 来发布您的本地提交）

执行`git push`将更改推送至远程仓库。

<span id="3"/>
### 分支
<span id="3.1"/>
#### 创建/删除/切换分支
创建一个dev分支，并切换至它。
```
git checkout -b dev
```
该命令等价于
```
git branch dev
git checkout dev
```
删除dev分支
```
git branch -d dev
```
<span id="3.2"/>
#### 合并某分支至当前分支
```
git merge <分支>
```
有合并就会有冲突，解决分支合并冲突的方法同上节。


...待续...
