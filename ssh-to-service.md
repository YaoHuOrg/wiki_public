# 使用ssh推送代码到本服务的配置方法

## 整理人：郭都豪（大二大数据）

### 生成ssh密钥

1. 执行如下命令：
```
ssh-keygen -t rsa -b 4096 -C "your_email@example.com"
# Creates a new ssh key, using the provided email as a label
Generating public/private rsa key pair.
```
2. 系统会询问将密钥保存在哪里，直接按下回车键保存在默认位置
```
Enter a file in which to save the key (/Users/you/.ssh/id_rsa): [Press enter]
```
3. 设置一个密码，并重复
```
Enter passphrase (empty for no passphrase): [Type a passphrase]
Enter same passphrase again: [Type passphrase again]
```
4. 将ssh-key添加到ssh-agent
```
ssh-add ~/.ssh/id_rsa
```
5. 打印public_key并将其添加到[管理 SSH 密钥](http://git.nyan.ac.cn/user/settings/ssh)
```
cat ~/.ssh/id_rsa.pub
```
