﻿
# C# 代码规范

## 首先阅读[通用规范](general-rules.md)
---

## 整理人:马文瑞（大一软工）

### 1.类名使用Pascal 大小写形式 
```
public class HelloWorld{ ... }
```

### 2.方法（属性）使用Pascal 大小写形式 
```
public class HelloWorld{ void SayHello(string name) { ... } }
```

### 3.用Camel规则来命名局部变量和方法的参数. 
```
class HelloWorld
{ 
    int toTalount = 0; 
	void SayHello(string name) 
    {  
	string fullMessage = "Hello " + name;  ... 
    }
}  
```

### 4.用有意义的，描述性的词语来命名变量 
尽量不要使用缩写。用name, address, salary等代替 nam, addr, sal (理解不清的要注释)
用于循环迭代的变量例外：
```
for ( int i = 0; i < count; i++ ){ ...}
```

### 5.所有的成员变量（另一种说法叫“字段”。请注意区分“字段(Field)”“属性(
Property)”与“特性(Attribute)”的区别）前加前缀 m_，常量每个字符都大写
```
public class Database
{
	public string m_connectionString;
}
```

### 6.接口的名称加前缀 I.
```
interface ICompare
{
	int compare();
}
```

### 7.自定义的特性以Attribute结尾
```
public class AuthorAttribute : Attribute
{
}
```

### 8.自定义的异常以Exception结尾
```
public class AppException : Exception
{
}
```

### 9.方法的命名.一般将其命名为动宾短语.
```
ShowDialog()
CreateFile()
GetPath()
```


### 10.文件名要和类名匹配 

例如，对于类HelloWorld, 相应的文件名应为 HelloWorld.cs

目录及文件的名字要取得有意义
 


### 11.缩进和间隔
+ 进用 TAB . 不用 SPACES.

+ 释需和代码对齐.

+ 括弧 ( {} ) 需和括号外的代码对齐。 

+ 一个空行来分开代码的逻辑分组.

+ if try 涉及{…}代码块要用一句话注释来简单解释

### 12.属性的使用（安全控制）
```
　public string Name
　{
　　get { return this.name; }
　　set { this.name = value; }
　}
```
如果你要在类外部给一个变量赋值的，要使用属性来赋值，这样具有更高的安全性

#### 注：由于最近的更新，我们更推荐以下写法：
```
　public string Name { get; set; }
```
　
#### 在最新的.net中还可以顺带赋予初值，如：
```
　public string Truth { get; set; } = "YHTZuiChunZhen";
```

### 13.使用某个控件的值时，尽量命名局部变量.

### 14.把引用的系统的namespace和自定义或第三方的分开.

### 15.目录结构中要反应出namespace的层次.

### 16.尽量不是用扩展类。如果真需如此，在使用扩展类是，保证其名称为 被扩展类名 + Expand.

### 17.尽量使用C#中自带的语法糖.

### 18.使用正则表达式时一定要有注释说明作用.

### 19.良好的编程习惯
+ 避免使用大文件。如果一个文件里的代码超过300～400行，必须考虑将代码分开到不同类中。 

+ 避免写太长的方法。一个典型的方法代码在1～25行之间。如果一个方法发代码超过25行，应该考虑将其分解为不同的方法。 

+ 方法名需能看出它作什么。别使用会引起误解的名字。如果名字一目了然，就无需用文档来解释方法的功能了。（然而这并不代表我们可以不为这个方法写注释。善用VS中的XML注释功能。该功能可使得代码有自解释特性，以实现去文档化开发。）

+ 别在程序中使用固定数值，用常量代替。 

+ 别用字符串常数。用资源文件。 

+ 避免使用很多成员变量。声明局部变量，并传递给方法。不要在方法间共享成员变量。如果在几个方法间共享一个成员变量，那就很难知道是哪个方法在什么时候修改了它的值。全局静态变量最好只读取，不要修改。 
+ 必要时使用enum 。别用数字或字符串来指示离散值。 
#### 好： 
```
 enum MailType 
{  Html,  PlainText,  Attachment } 
```
```
void SendMail (string message, MailType mailType) 
{  
	switch ( mailType )  
	{   case MailType.Html:    // Do something    		break;   
	    case MailType.PlainText: // Do something
		break;   
	   case MailType.Attachment:    // Do something    		break;   
	   default:    // Do something    break;  
	} 
}            
```

#### 不好： 
```
 void SendMail (string message, string mailType) 
{  
	switch ( mailType )  
	{
        case "Html":    // Do something    
			break;   
		case "PlainText":    // Do something   
			break;   
		case "Attachment":    // Do something    			break;   
		default:    // Do something    break;  
	} 
}
```

+ 别把成员变量声明为 public 或 protected。都声明为 private 

+ 不在代码中使用具体的路径和驱动器名。 使用相对路径，并使路径可编程。 

+ 错误消息需能帮助用户解决问题。永远别用象"应用程序出错", "发现一个错误" 等错误消息。而应给出象 "更新数据库失败。请确保登陆id和密码正确。" 的具体消息。

+ 显示给用户的消息要简短而友好。但要把所有可能的信息都记录下来，以助诊断问题。 

### 20.注释
+ 别每行代码，每个声明的变量都做注释。 

+ 在需要的地方注释。可读性强的代码需要很少的注释。如果所有的变量和方法的命名都很有意义，会使代码可读性很强并无需太多注释。 

+ 行数不多的注释会使代码看起来优雅。但如果代码不清晰，可读性差，那就糟糕。 

+ 如果因为某种原因使用了复杂艰涩的原理，为程序配备良好的文档和重分的注释。 

+ 对一个数值变量采用不是0,-1等的数值初始化，给出选择该值的理由。 

+ 简言之，要写清晰，可读的代码以致无须什么注释就能理解。

+ 以上为理论，但由于缺乏经验，我俱乐部成员可能难以达成，所以建议按照之后提出的标准进行详细且必要的注释。



### 21.异常处理
+ 不要“捕捉了异常却什么也不做“。如果隐藏了一个异常，你将永远不知道异常到底发生了没有。 

+ 发生异常时，给出友好的消息给用户，但要精确记录错误的所有可能细节，包括发生的时间，和相关方法，类名等。 

+ 只捕捉特定的异常，而不是一般的异常。

#### 好： 
```
 void ReadFromFie ( string fileName ) 
{  
	try  {   // read from file.  }  
	catch (FileIOException ex)  
	{   
		// log error.   
		//  re-throw exception depending on your 		case.   throw;  
	} 
}
```
#### 不好： 
```
 void ReadFromFile ( string fileName ) 
{  
	try  {   // read from file.  }  
	catch (Exception ex)   
	{   
// Catching general exception is bad... we will never know whether it   
// was a file error or some other error.      
// Here you are hiding an exception.    
// In this case no one will ever know that an exception happened.   
		return;    
	} 
}
```

+ 你可以用应用程序级（线程级）错误处理器处理所有一般的异常。遇到“意外的一般性错误”时，此错误处理器应该捕捉异常，给用户提示消息，在应用程序关闭或 用户选择”忽略并继续“之前记录错误信息。 

+ 不必每个方法都用try-catch。当特定的异常可能发生时才使用。比如，当你写文件时，处理异常FileIOException. 

+ 别写太大的 try-catch 模块。如果需要，为每个执行的任务编写单独的 try-catch 模块。 这将帮你找出哪一段代码产生异常，并给用户发出特定的错误消息。


___


# 除此以外，本规范还做如下声明：
### 1、代码格式规范按照Microsoft Visual Studio最新版（现为2015）的自动格式进行要求（输入;一般可进行自动排版）
### 2、根据第一条，我俱乐部C#语言大括号使用规则与我俱乐部Java语言规范中不同。当大括号代码块涉及多行时，大括号独占一行并与上一行、反括号缩进对齐。
### 3、在使用观察者（设计）模式时，在声明事件时，建议遵循.net语言规范（详见百度百科 [C#委托](http://baike.baidu.com/link?url=ElAz8xq_XfU2xIsaS6weCSt_HYk_fWSAok9Bya-k2MAig6p7ajnvBksTXCJhlAMkSTYWj7zQ_Ej_SwMeNhwqha#6) 词条 中的 7委托事件），但不做强求。
### 4、强制要求在每个公开类及其公开方法前使用“///”自动生成微软xml格式注释，并至少使用一句话描述下该类/方法的功能。（事实上，根据我的经验，我建议大家对每个方法和类前都如此操作。这样可以将其自动注入到Visual Studio的代码提示中。这对合作开发非常重要。即使是私人项目，这对日后的维护也相当重要。毕竟人是会遗忘的。）
### 5、尽量保证代码中30%以上的注释量。
### 6、本规范没有声明的地方参见本俱乐部java语言规范与微软出品的CSharp Language Specification，前者优先。

___

## 声明：本规范由网络上的C#规范修订而来，更改了其中的一些错误，增加了最近.net中的新语法特性的规范，并根据本俱乐部情况进行了修改。原作者不可考，在此向其致敬！
