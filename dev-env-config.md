# 关于开发环境配置

### Windows
- 使用WNMP整合包，具体询问`袁海天`或`马文瑞`。
- 编辑器建议使用`Sublime Text`。
### Linux/OS X
- 在开发环境中使用`LAMP`(Linux,apache,mysql,php)。
- 为了更好的性能，建议在生产及测试环境中使用`LNMP`(Linux,nginx,mysql,php)。
- nginx使用`php7.0-fpm`或`hhvm`作为cgi接口。
在终端执行如下命令：
```
sudo apt install apache2 php7.0 libapache2-mod-php7.0 mysql-server php7.0-mysql
```
然后按需要安装php模块。

- 编辑器建议使用`vim`。
我使用的vim配置：https://github.com/tlhunter/vimrc 
简明vim教程：https://zhuanlan.zhihu.com/p/22530297 

**这周五将进入正式开发，请务必在周末之前将环境配置完成**
