﻿# 主页
### [文档首页](http://git.nyan.ac.cn/bjut-ibm-club/wiki) 

# 代码规范
* [通用规范](general-rules.md)
* [Java编码规范](code-style-java.md)
* [Python编码规范](code-style-python.md)
* [Object-C编码规范](code-style-object-c.md)
* [C#编码规范](code-style-csharp.md)
* [C编码规范](code-style-c.md)
* [前端编码规范](code-style-front-end.md)


# 教程
## Git版本库相关
* [使用ssh推送代码到本服务的配置方法](ssh-to-service.md)
* [关于linux下git shell的使用方法](git_shell.md)
* [git学习笔记](git-totorial.md)
* [再见，命令行！- 图形化git软件SourceTree的使用入门](sourcetree.md)

