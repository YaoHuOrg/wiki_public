﻿
# 可视化Git管理软件SourceTree使用入门
### 提供人：马文瑞（大一软工）
### 原创内容，限部内使用。 如有问题，百度无果，欢迎找我讨论。
<br />

---
### 0.声明
本教程为入门教程，旨在帮助新人避免一些比较浪费时间的错误并降低理解成本。由于SourceTree自带中文，希望同学们对于其他高级功能自行理解并善用百度。由于SourceTree的下载服务器需要翻墙，本俱乐部提供下载资源，但是仍然建议同学们自学科学上网。除此以外，为了方便同学理解本教程，希望事先学习本俱乐部的[git-shell教程](git_shell.md)及其中附上的[链接](http://www.bootcss.com/p/git-guide/)，以对git的工作方式有所了解。


### 1.SourceTree的安装
由于SourceTree相关资源下载非常麻烦，我已经下载完成放到俱乐部百度网盘共享了。欢迎下载
#### 1.1 前置软件的安装
SourceTree需要Git以及Mercurial的支持，故下面我们介绍这两款软件的安装方式。
<br />
##### 1.1.1 Git Bash的安装
其实SourceTree服务器可以自动下载Git，单由于国内网络访问不了该服务器，网速也很慢，链接极易丢失，所以建议大家手动下载安装。
<br />
Git的安装设置选项很多。但是建议初学者按照默认设置安装即可。
##### 1.1.2 Mercurial的安装
Mercurial因为Git同样的缘由，建议大家自行下载安装。Mercurial的安装较为简单，在这里不多做介绍了。
##### 1.1.3 SourceTree的安装与初始化设置
SourceTree安装流程单一，但是有几点一定要注意。
<br />
首先，SourceTree安装过程建议全程翻墙，并且提前[注册账号](https://id.atlassian.com/login)，并[在此获得免费试用许可](https://id.atlassian.com/login?application=mac&continue=https://my.atlassian.com/license/sourcetree)。获得免费许可后登陆账号即可继续设置。
<br />
在为Git指定位置时，应指定到安装目录下cmd目录的git.exe，否则会失败，请特别注意。
<br />
PS.新建库和克隆库两步可以暂时跳过。



### 2.SourceTree的使用
#### 2.1 新建/克隆版本库
##### 2.1.1 在远端建立新版本库(克隆版本库请跳过此步)
![create1](img/sourcetree/create1.jpg)
<br />
![create2](img/sourcetree/create2.png)
对于可见性这一个选项我在这里进行说明：
<br />
在公有(public权限，即完全开源项目)中

|用户                                 |查看      |修改       |分支     |派生      |增加成员
|-------------------------------------|----------|-----------|---------|----------|------------
|创建者/拥有者                        |√        |√         |√       |√        |√
|协助者                               |√        |√         |√       |√        |×
|会员                                 |√        |须经同意   |×       |√        |×
|访客（然而本俱乐部Git禁止了访客访问）|√        |×         |×       |×        |×


在私有(private权限)中

|用户                                 |查看      |修改       |分支     |派生      |增加成员
|-------------------------------------|----------|-----------|---------|----------|------------
|创建者/拥有者                        |√        |√         |√       |√        |√
|协助者                               |√        |√         |√       |√        |×
|会员                                 |×        |×         |×       |×        |×
|访客（然而本俱乐部Git禁止了访客访问）|×        |×         |×       |×        |×


##### 2.1.2 获取版本库HTTP地址
创建后打开版本库页面

![gethtml](img/sourcetree/gethtml.png)

注意紫框中的内容，需选择html并复制后面的链接。
##### 2.1.3 在SourceTree中克隆版本库
![clone](img/sourcetree/clone.jpg)
#### 2.2 基础操作
![introduction](img/sourcetree/introduction.jpg)
#### 2.3 提交注意事项
![commit](img/sourcetree/commit.jpg)