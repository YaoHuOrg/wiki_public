﻿# 关于linux下git shell的使用方法

## [很好的教程,参考阅读](http://www.bootcss.com/p/git-guide/) 

## 提供者：袁海天（大二嵌入）

## 1. 获取项目的ssh链接:

* 点击红框复制或者直接复制ssh链接，复制的内容下文简称为clone_path

![example_clone](img/git_shell/example_clone.jpg)

* 在想要编辑的目录下打开terminal，输入命令： `git clone clone_path`

![clone](img/git_shell/clone.jpg)

* 输入命令：  `cd 项目名称` 举例，如果项目名字是`wiki`，`cd wiki`进入项目

## 2.操作项目

* 如果需要切换分支（自己的分支而不是master，develop），输入命令：`git checkout 分支名`

![checkout](img/git_shell/checkout.jpg)

* 进入自己的项目后，可以修改，增删文件，然后输入命令： `git add . `(have .)

* 输入命令： `git status` 可以查看变化.

## 3.上传修改后的项目

![commit](img/git_shell/addcommit.jpg)

* 输入命令：`git commit -m "修改的陈述"`

* 输入命令：`git push origin 分支名(比如master)` 上传

![push](img/git_shell/push.jpg)
